import UIKit
import core

public extension UIColor {
    static var primary: UIColor { return UIColor(hex: "#33907C") }
    static var title: UIColor { return UIColor(hex: "#001A41") }
    static var grey20: UIColor { return UIColor(hex: "#EDECF0") }
}
