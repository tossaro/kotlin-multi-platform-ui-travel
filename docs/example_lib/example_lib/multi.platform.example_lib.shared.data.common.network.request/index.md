//[example_lib](../../index.md)/[multi.platform.example_lib.shared.data.common.network.request](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [RefreshTokenReq](-refresh-token-req/index.md) | [common]<br>@Serializable()<br>public final class [RefreshTokenReq](-refresh-token-req/index.md) |
