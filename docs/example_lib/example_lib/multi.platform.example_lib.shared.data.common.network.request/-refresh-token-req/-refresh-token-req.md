//[example_lib](../../../index.md)/[multi.platform.example_lib.shared.data.common.network.request](../index.md)/[RefreshTokenReq](index.md)/[RefreshTokenReq](-refresh-token-req.md)

# RefreshTokenReq

[common]\

public [RefreshTokenReq](index.md)[RefreshTokenReq](-refresh-token-req.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)msisdn)
