//[example_lib](../../../index.md)/[multi.platform.example_lib.shared.data.common.network.request](../index.md)/[RefreshTokenReq](index.md)

# RefreshTokenReq

[common]\
@Serializable()

public final class [RefreshTokenReq](index.md)

## Constructors

| | |
|---|---|
| [RefreshTokenReq](-refresh-token-req.md) | [common]<br>public [RefreshTokenReq](index.md)[RefreshTokenReq](-refresh-token-req.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)msisdn) |

## Functions

| Name | Summary |
|---|---|
| [getMsisdn](get-msisdn.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getMsisdn](get-msisdn.md)() |

## Properties

| Name | Summary |
|---|---|
| [msisdn](index.md#244533744%2FProperties%2F-1932516659) | [common]<br>private final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[msisdn](index.md#244533744%2FProperties%2F-1932516659) |
