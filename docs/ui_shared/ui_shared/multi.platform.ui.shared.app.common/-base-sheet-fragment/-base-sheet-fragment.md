//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseSheetFragment](index.md)/[BaseSheetFragment](-base-sheet-fragment.md)

# BaseSheetFragment

[android]\

public [BaseSheetFragment](index.md)&lt;[B](index.md)&gt;[BaseSheetFragment](-base-sheet-fragment.md)(@[LayoutRes](https://developer.android.com/reference/kotlin/androidx/annotation/LayoutRes.html)()[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)layoutResId)
