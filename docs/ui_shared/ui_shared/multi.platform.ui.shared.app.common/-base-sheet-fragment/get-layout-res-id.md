//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseSheetFragment](index.md)/[getLayoutResId](get-layout-res-id.md)

# getLayoutResId

[android]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getLayoutResId](get-layout-res-id.md)()
