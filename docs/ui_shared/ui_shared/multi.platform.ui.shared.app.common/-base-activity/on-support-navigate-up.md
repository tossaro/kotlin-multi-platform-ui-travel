//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseActivity](index.md)/[onSupportNavigateUp](on-support-navigate-up.md)

# onSupportNavigateUp

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[onSupportNavigateUp](on-support-navigate-up.md)()
