//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseActivity](index.md)/[onBackPressed](on-back-pressed.md)

# onBackPressed

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBackPressed](on-back-pressed.md)()
