//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseActivity](index.md)

# BaseActivity

[android]\
public abstract class [BaseActivity](index.md) extends CoreActivity

## Constructors

| | |
|---|---|
| [BaseActivity](-base-activity.md) | [android]<br>public [BaseActivity](index.md)[BaseActivity](-base-activity.md)() |

## Functions

| Name | Summary |
|---|---|
| [actionBar](action-bar.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBar](action-bar.md)()<br>Open function for override action bar binding Default: null |
| [actionBarCollapsingLayout](action-bar-collapsing-layout.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarCollapsingLayout](action-bar-collapsing-layout.md)()<br>Open function for override action bar collapsing layout binding Default: null |
| [actionBarExpandedAutoComplete](action-bar-expanded-auto-complete.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedAutoComplete](action-bar-expanded-auto-complete.md)()<br>Open function for override action bar auto complete binding Default: null |
| [actionBarExpandedDescription](action-bar-expanded-description.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedDescription](action-bar-expanded-description.md)()<br>Open function for override action bar description binding Default: null |
| [actionBarExpandedDotIndicator](action-bar-expanded-dot-indicator.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedDotIndicator](action-bar-expanded-dot-indicator.md)()<br>Open function for override action bar tab layout for dot indicator binding Default: null |
| [actionBarExpandedInfoIcon](action-bar-expanded-info-icon.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedInfoIcon](action-bar-expanded-info-icon.md)()<br>Open function for override action bar info icon binding Default: null |
| [actionBarExpandedInfoText](action-bar-expanded-info-text.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedInfoText](action-bar-expanded-info-text.md)()<br>Open function for override action bar info text binding Default: null |
| [actionBarExpandedViewPager](action-bar-expanded-view-pager.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarExpandedViewPager](action-bar-expanded-view-pager.md)()<br>Open function for override action bar viewpager binding Default: null |
| [actionBarLayout](action-bar-layout.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarLayout](action-bar-layout.md)()<br>Open function for override action bar layout binding Default: null |
| [actionBarSearch](action-bar-search.md) | [android]<br>public &lt;Error class: unknown class&gt;[actionBarSearch](action-bar-search.md)()<br>Open function for override action bar search binding Default: null |
| [bottomNavBar](bottom-nav-bar.md) | [android]<br>public &lt;Error class: unknown class&gt;[bottomNavBar](bottom-nav-bar.md)()<br>Open function for override bottom navigation bar binding Default: null |
| [clRoot](cl-root.md) | [android]<br>public &lt;Error class: unknown class&gt;[clRoot](cl-root.md)()<br>Open function for override action bar binding Default: null |
| [navHostFragment](nav-host-fragment.md) | [android]<br>public [NavHostFragment](https://developer.android.com/reference/kotlin/androidx/navigation/fragment/NavHostFragment.html)[navHostFragment](nav-host-fragment.md)()<br>Open function for container navigation host binding |
| [onBackPressed](on-back-pressed.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBackPressed](on-back-pressed.md)() |
| [onSupportNavigateUp](on-support-navigate-up.md) | [android]<br>public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[onSupportNavigateUp](on-support-navigate-up.md)() |
| [showSnackbar](show-snackbar.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSnackbar](show-snackbar.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)messageString, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isError, [View](https://developer.android.com/reference/kotlin/android/view/View.html)anchor)<br>Open function for snackbar message |
