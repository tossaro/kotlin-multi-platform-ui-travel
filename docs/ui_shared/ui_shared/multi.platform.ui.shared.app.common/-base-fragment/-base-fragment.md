//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseFragment](index.md)/[BaseFragment](-base-fragment.md)

# BaseFragment

[android]\

public [BaseFragment](index.md)&lt;[B](index.md)&gt;[BaseFragment](-base-fragment.md)(@[LayoutRes](https://developer.android.com/reference/kotlin/androidx/annotation/LayoutRes.html)()[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)layoutResId)
