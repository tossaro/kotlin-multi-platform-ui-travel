//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseFragment](index.md)/[onDestroy](on-destroy.md)

# onDestroy

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onDestroy](on-destroy.md)()
