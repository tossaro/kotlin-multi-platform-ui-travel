//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseFragment](index.md)/[onResume](on-resume.md)

# onResume

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onResume](on-resume.md)()
