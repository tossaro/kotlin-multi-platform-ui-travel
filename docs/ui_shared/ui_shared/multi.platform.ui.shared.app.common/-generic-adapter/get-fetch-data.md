//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[GenericAdapter](index.md)/[getFetchData](get-fetch-data.md)

# getFetchData

[android]\

public final Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[getFetchData](get-fetch-data.md)()
