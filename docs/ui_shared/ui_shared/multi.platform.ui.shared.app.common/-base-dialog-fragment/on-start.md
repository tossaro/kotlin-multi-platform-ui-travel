//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseDialogFragment](index.md)/[onStart](on-start.md)

# onStart

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onStart](on-start.md)()
