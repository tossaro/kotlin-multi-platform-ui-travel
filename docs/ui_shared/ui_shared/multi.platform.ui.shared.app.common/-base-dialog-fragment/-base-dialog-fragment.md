//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.common](../index.md)/[BaseDialogFragment](index.md)/[BaseDialogFragment](-base-dialog-fragment.md)

# BaseDialogFragment

[android]\

public [BaseDialogFragment](index.md)&lt;[B](index.md)&gt;[BaseDialogFragment](-base-dialog-fragment.md)(@[LayoutRes](https://developer.android.com/reference/kotlin/androidx/annotation/LayoutRes.html)()[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)layoutResId)
