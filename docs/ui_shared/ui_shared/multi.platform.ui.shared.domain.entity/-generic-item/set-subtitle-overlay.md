//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setSubtitleOverlay](set-subtitle-overlay.md)

# setSubtitleOverlay

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSubtitleOverlay](set-subtitle-overlay.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)subtitleOverlay)
