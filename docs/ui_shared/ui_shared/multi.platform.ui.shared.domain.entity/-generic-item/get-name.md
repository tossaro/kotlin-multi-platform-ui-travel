//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getName](get-name.md)

# getName

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)()
