//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getDescriptionIconRes](get-description-icon-res.md)

# getDescriptionIconRes

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getDescriptionIconRes](get-description-icon-res.md)()
