//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[setTopImage](set-top-image.md)

# setTopImage

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTopImage](set-top-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)topImage)
