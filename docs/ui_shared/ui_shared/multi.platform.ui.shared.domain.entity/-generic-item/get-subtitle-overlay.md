//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getSubtitleOverlay](get-subtitle-overlay.md)

# getSubtitleOverlay

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getSubtitleOverlay](get-subtitle-overlay.md)()
