//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getDescription](get-description.md)

# getDescription

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getDescription](get-description.md)()
