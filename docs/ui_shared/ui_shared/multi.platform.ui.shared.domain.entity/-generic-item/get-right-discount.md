//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getRightDiscount](get-right-discount.md)

# getRightDiscount

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getRightDiscount](get-right-discount.md)()
