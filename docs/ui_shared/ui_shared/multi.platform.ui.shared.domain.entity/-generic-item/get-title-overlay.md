//[ui_shared](../../../index.md)/[multi.platform.ui.shared.domain.entity](../index.md)/[GenericItem](index.md)/[getTitleOverlay](get-title-overlay.md)

# getTitleOverlay

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getTitleOverlay](get-title-overlay.md)()
