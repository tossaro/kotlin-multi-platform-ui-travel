//[ui_shared](../../index.md)/[multi.platform.ui.shared.app.datesheet](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [DateSheetFragment](-date-sheet-fragment/index.md) | [android]<br>public final class [DateSheetFragment](-date-sheet-fragment/index.md) extends [BaseSheetFragment](../multi.platform.ui.shared.app.common/-base-sheet-fragment/index.md)&lt;&lt;Error class: unknown class&gt;&gt; |
