//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.gallery](../index.md)/[GalleryAdapter](index.md)/[getItemCount](get-item-count.md)

# getItemCount

[android]\

public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getItemCount](get-item-count.md)()
