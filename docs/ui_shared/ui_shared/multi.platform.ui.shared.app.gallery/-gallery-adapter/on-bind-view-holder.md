//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.gallery](../index.md)/[GalleryAdapter](index.md)/[onBindViewHolder](on-bind-view-holder.md)

# onBindViewHolder

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBindViewHolder](on-bind-view-holder.md)([GalleryAdapter.ViewHolder](-view-holder/index.md)viewHolder, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position)
