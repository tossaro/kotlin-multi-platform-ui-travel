//[ui_shared](../../../index.md)/[multi.platform.ui.shared.app.gallery](../index.md)/[GalleryAdapter](index.md)/[getItems](get-items.md)

# getItems

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getItems](get-items.md)()
