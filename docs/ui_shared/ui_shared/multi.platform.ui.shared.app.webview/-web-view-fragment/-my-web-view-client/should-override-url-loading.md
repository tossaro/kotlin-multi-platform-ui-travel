//[ui_shared](../../../../index.md)/[multi.platform.ui.shared.app.webview](../../index.md)/[WebViewFragment](../index.md)/[MyWebViewClient](index.md)/[shouldOverrideUrlLoading](should-override-url-loading.md)

# shouldOverrideUrlLoading

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[shouldOverrideUrlLoading](should-override-url-loading.md)([WebView](https://developer.android.com/reference/kotlin/android/webkit/WebView.html)view, [WebResourceRequest](https://developer.android.com/reference/kotlin/android/webkit/WebResourceRequest.html)request)
