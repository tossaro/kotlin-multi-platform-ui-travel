package multi.platform.example_lib.shared.domain.profile

import multi.platform.core.shared.data.common.network.response.CoreResponse
import multi.platform.example_lib.shared.domain.profile.entity.Profile

@Suppress("kotlin:S6517")
interface ProfileRepository {
    suspend fun getProfile(accessToken: String?): CoreResponse<Profile?>?
}