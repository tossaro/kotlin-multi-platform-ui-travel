package multi.platform.example_lib.shared.data.common.network.request

import kotlinx.serialization.Serializable

@Serializable
data class RefreshTokenReq(
    val msisdn: String? = null,
)
