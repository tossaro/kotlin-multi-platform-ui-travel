Pod::Spec.new do |spec|
    spec.name                     = 'ui_shared'
    spec.version                  = '0.1.0'
    spec.homepage                 = 'https://gitlab.com/tossaro/kotlin-multi-platform-ui-travel'
    spec.source                   = { :http=> ''}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Provide UI Style & Component'
    spec.vendored_frameworks      = 'ui_shared/build/XCFrameworks/release/ui_shared.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.1'
end