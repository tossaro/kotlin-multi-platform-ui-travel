package multi.platform.ui.shared.app.common

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.KeyEvent.KEYCODE_BACK
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import multi.platform.core.shared.app.common.CoreDialogFragment
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.ui.shared.R
import multi.platform.ui.shared.databinding.BaseDialogFragmentBinding
import org.koin.core.component.inject

open class BaseDialogFragment<B : ViewDataBinding>(
    @LayoutRes val layoutResId: Int
) : CoreDialogFragment() {

    lateinit var binding: B
    private var fragmentView: View? = null
    private var _baseBinding: BaseDialogFragmentBinding? = null
    private val baseBinding get() = _baseBinding!!
    protected val sharedPreferences: SharedPreferences by inject()

    /**
     * Open function for override visibility loading binding
     */
    @Suppress("SameParameterValue")
    protected open fun showFullLoading(isShow: Boolean? = true) {
        hideKeyboard()
        baseBinding.loadingCircle.updateLayoutParams<ViewGroup.LayoutParams> {
            height = baseBinding.clDialogRoot.height
        }
        isShow?.let { s -> baseBinding.loadingCircle.isVisible = s }
    }

    override fun getRootLayoutRes(): Int = R.layout.base_dialog_fragment

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(isCancelable)
        dialog.setCanceledOnTouchOutside(isCancelable)
        dialog.setOnKeyListener { _, i, _ ->
            i == KEYCODE_BACK && !isCancelable
        }
        return dialog
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _baseBinding = BaseDialogFragmentBinding.inflate(inflater, container, false)
        (fragmentView?.parent as? ViewGroup)?.removeAllViews()
        fragmentView = inflater.inflate(getRootLayoutRes(), container, false).apply {
            binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
            baseBinding.fragmentContent.removeAllViews()
            baseBinding.fragmentContent.addView(binding.root)
        }
        baseBinding.ivClose.visibility = if (showCloseButton()) View.VISIBLE else View.GONE
        baseBinding.ivClose.setOnClickListener { dismiss() }
        binding.root.setOnApplyWindowInsetsListener { _, windowInsets ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val imeHeight = windowInsets.getInsets(WindowInsets.Type.ime()).bottom
                binding.root.setPadding(0, 0, 0, imeHeight)
            }
            windowInsets
        }
        return binding.root.rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _baseBinding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        LocaleUtil.onAttach(context)
    }

}