package multi.platform.ui.shared.external.extension

import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.ui.shared.app.common.BaseActivity

/**
 * Extension for show snackbar
 */
@Suppress("kotlin:S1871")
fun Fragment.showSnackbar(messageString: String?, isError: Boolean) {
    hideKeyboard()
    messageString?.let {
        val anchor: View? = if (this is DialogFragment) dialog?.window?.decorView else null
        (requireActivity() as BaseActivity).showSnackbar(it, isError, anchor)
    }
}


/**
 * Extension for show success snackbar
 */
fun Fragment.showSuccessSnackbar(messageString: String?) = showSnackbar(messageString, false)

/**
 * Extension for show error snackbar
 */
fun Fragment.showErrorSnackbar(messageString: String?) = showSnackbar(messageString, true)