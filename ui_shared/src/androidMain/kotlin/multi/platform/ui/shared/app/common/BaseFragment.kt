@file:Suppress("EmptyMethod")

package multi.platform.ui.shared.app.common

import android.content.Context
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.appbar.AppBarLayout
import multi.platform.core.shared.app.common.CoreFragment
import multi.platform.core.shared.external.constant.AppConstant
import multi.platform.core.shared.external.extension.dpToPx
import multi.platform.core.shared.external.extension.hideKeyboard
import multi.platform.core.shared.external.utility.LocaleUtil
import multi.platform.ui.shared.R
import multi.platform.ui.shared.databinding.BaseFragmentBinding
import org.koin.core.component.inject


open class BaseFragment<B : ViewDataBinding>(
    @LayoutRes val layoutResId: Int
) : CoreFragment() {

    lateinit var binding: B
    private var fragmentView: View? = null
    private var _baseBinding: BaseFragmentBinding? = null
    private val baseBinding get() = _baseBinding!!
    protected val sharedPreferences: SharedPreferences by inject()
    private var isTopInsets = true
    private var isBottomInsets = true
    private var onActionOffsetChanged: AppBarLayout.OnOffsetChangedListener? = null

    /**
     * Open function for override root layout resource
     * Default: R.layout.base_fragment
     */
    protected open fun getRootLayoutRes() = R.layout.base_fragment

    protected open fun actionBar() = (requireActivity() as BaseActivity).actionBar()

    /**
     * Open function for override action bar search binding
     * Default: baseActivity.actionBarSearch
     */
    protected open fun actionBarSearch() = (requireActivity() as BaseActivity).actionBarSearch()

    /**
     * Open function for override action bar layout binding
     * Default: baseActivity.actionBarLayout
     */
    protected open fun actionBarLayout() = (requireActivity() as BaseActivity).actionBarLayout()

    /**
     * Open function for override action bar content scrim
     * Default: R.color.white
     */
    protected open fun actionBarContentScrim() = R.color.white

    /**
     * Open function for override action bar collapsing layout binding
     * Default: baseActivity.actionBarCollapsingLayout
     */
    protected open fun actionBarCollapsingLayout() =
        (requireActivity() as BaseActivity).actionBarCollapsingLayout()

    /**
     * Open function for override action bar auto complete binding
     * Default: baseActivity.actionBarViewPager
     */
    protected open fun actionBarExpandedDescription() =
        (requireActivity() as BaseActivity).actionBarExpandedDescription()

    /**
     * Open function for override action bar auto complete binding
     * Default: baseActivity.actionBarViewPager
     */
    protected open fun actionBarExpandedAutoComplete() =
        (requireActivity() as BaseActivity).actionBarExpandedAutoComplete()

    /**
     * Open function for override action bar viewpager binding
     * Default: baseActivity.actionBarExpandedViewPager
     */
    protected open fun actionBarExpandedViewPager() =
        (requireActivity() as BaseActivity).actionBarExpandedViewPager()

    /**
     * Open function for override action bar tab layout for dot indicator binding
     * Default: baseActivity.actionBarExpandedDotIndicator
     */
    protected open fun actionBarExpandedDotIndicator() =
        (requireActivity() as BaseActivity).actionBarExpandedDotIndicator()

    /**
     * Open function for override action bar info icon binding
     * Default: baseActivity.actionBarExpandedInfoIcon
     */
    protected open fun actionBarExpandedInfoIcon() =
        (requireActivity() as BaseActivity).actionBarExpandedInfoIcon()

    /**
     * Open function for override action bar info text binding
     * Default: baseActivity.actionBarInfoText
     */
    protected open fun actionBarExpandedInfoText() =
        (requireActivity() as BaseActivity).actionBarExpandedInfoText()

    /**
     * Open function for override bottom navigation bar binding
     * Default: baseActivity.bottomNav
     */
    protected open fun bottomNavBar() = (requireActivity() as BaseActivity).bottomNavBar()

    /**
     * Open function for override visibility loading binding
     */
    protected open fun showFullLoading(visibility: Boolean? = true) {
        hideKeyboard()
        visibility?.let { baseBinding.loadingLinear.isVisible = it }
    }

    override fun navBarColor() = R.color.navBar

    override fun statusBarColor() = R.color.statusBar

    protected open fun setActionBarOffsetListener() {
        if (isActionBarOffsetListenerEnable() || !showActionBarTitleOnExpanded()) {
            var isShow = true
            var scrollRange = -1
            onActionOffsetChanged =
                AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                    if (scrollRange == -1) scrollRange = appBarLayout?.totalScrollRange!!
                    if (scrollRange + verticalOffset == 0) {
                        actionBarCollapsingLayout()?.title = actionBarTitle()
                        onCollapsedActionBar()
                        isShow = true
                    } else if (isShow) {
                        if (!showActionBarTitleOnExpanded()) actionBarCollapsingLayout()?.title =
                            " "
                        onExpandedActionBar()
                        isShow = false
                    }
                }
            actionBarLayout()?.addOnOffsetChangedListener(onActionOffsetChanged)
        }
    }

    protected open fun setActionBarSearchIcon() {
        actionBarSearch()?.apply {
            val icSearch = ContextCompat.getDrawable(requireContext(), R.drawable.ic_search)
            var icFilter: Drawable? = null
            if (showActionBarSearchFilter()) {
                icFilter = ContextCompat.getDrawable(requireContext(), R.drawable.ic_filter)
                val isFiltered = sharedPreferences.getBoolean(AppConstant.FILTERED_KEY, false)
                val tintColor = if (isFiltered) R.color.primary else R.color.blue100
                icFilter?.let {
                    DrawableCompat.setTint(it, ContextCompat.getColor(requireContext(), tintColor))
                }
            }
            setCompoundDrawablesWithIntrinsicBounds(icSearch, null, icFilter, null)
        }
    }

    protected open fun setActionBarVisibility() {
        if (showActionBar()) {
            actionBarLayout()?.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                height = dpToPx(actionBarHeight().toFloat())
            }
        } else {
            actionBarLayout()?.updateLayoutParams<CoordinatorLayout.LayoutParams> { height = 0 }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        LocaleUtil.onAttach(context)
    }

    @Suppress("ClickableViewAccessibility")
    override fun onResume() {
        super.onResume()
        activity?.window?.run {
            if (isFullScreen()) {
                isTopInsets = false
                isBottomInsets = false
            }

            setInsetsListener(this)
            setNavigationBar(this)
            setResize(this)
            statusBarColor = ContextCompat.getColor(requireContext(), statusBarColor())
            if (doesFitSystemWindows()) WindowCompat.setDecorFitsSystemWindows(this, true)
        }

        actionBarSearch()?.apply {
            isFocusable = true
            isFocusableInTouchMode = true
            isVisible = showActionBarSearch()
            setOnTouchListener { _, motionEvent ->
                if (motionEvent.action == MotionEvent.ACTION_UP
                    && this.compoundDrawables[2] != null
                    && motionEvent.rawX >= (this.right - this.compoundDrawables[2].bounds.width())
                ) {
                    onTapFilterActionBarSearch()
                    return@setOnTouchListener true
                }
                false
            }
        }
        actionBarExpandedInfoText()?.isVisible = showActionBarInfo()
        bottomNavBar()?.isVisible = showBottomNavBar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionBarLayout()?.setExpanded(expandActionBar(), expandActionBar())
        val appBarParams = actionBarLayout()?.layoutParams as CoordinatorLayout.LayoutParams
        if (appBarParams.behavior == null) appBarParams.behavior = AppBarLayout.Behavior()
        val appBarBehaviour = appBarParams.behavior as AppBarLayout.Behavior
        appBarBehaviour.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                return expandActionBar()
            }
        })
        setActionBarVisibility()
        setActionBarSearchIcon()
        actionBarCollapsingLayout()?.title = actionBarTitle()
        actionBarCollapsingLayout()?.setContentScrimResource(actionBarContentScrim())
        setActionBarOffsetListener()
        actionBar()?.apply {
            setPadding(dpToPx(16f), 0, dpToPx(16f), 0)
            updateLayoutParams<ViewGroup.MarginLayoutParams> {
                topMargin = dpToPx(actionBarTopMargin().toFloat())
            }
        }
        actionBarSearch()?.apply {
            val dp11 = dpToPx(11f)
            setPadding(dp11, dp11, dp11, dp11)
            textSize = 16f
            height = dpToPx(40f)
            gravity = Gravity.START or Gravity.CENTER_VERTICAL
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_baseBinding == null) {
            _baseBinding = BaseFragmentBinding.inflate(inflater, container, false)
            (fragmentView?.parent as? ViewGroup)?.removeAllViews()
            fragmentView = inflater.inflate(getRootLayoutRes(), container, false).apply {
                binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
                baseBinding.fragmentContent.removeAllViews()
                baseBinding.fragmentContent.addView(binding.root)
            }
        }
        return binding.root.rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        _baseBinding = null
    }

    @Suppress("ClickableViewAccessibility")
    override fun onStop() {
        super.onStop()
        actionBarExpandedDotIndicator()?.removeAllTabs()
        actionBarExpandedViewPager()?.adapter = null
        actionBarSearch()?.setOnTouchListener(null)
        if (!showActionBarTitleOnExpanded()) {
            actionBarLayout()?.removeOnOffsetChangedListener(onActionOffsetChanged)
        }
    }
}