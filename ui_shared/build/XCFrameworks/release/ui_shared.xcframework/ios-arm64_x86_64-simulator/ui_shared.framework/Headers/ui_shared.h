#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class Ui_sharedGenericItem;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface Ui_sharedBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end

@interface Ui_sharedBase (Ui_sharedBaseCopying) <NSCopying>
@end

__attribute__((swift_name("KotlinMutableSet")))
@interface Ui_sharedMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end

__attribute__((swift_name("KotlinMutableDictionary")))
@interface Ui_sharedMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end

@interface NSError (NSErrorUi_sharedKotlinException)
@property (readonly) id _Nullable kotlinException;
@end

__attribute__((swift_name("KotlinNumber")))
@interface Ui_sharedNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end

__attribute__((swift_name("KotlinByte")))
@interface Ui_sharedByte : Ui_sharedNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end

__attribute__((swift_name("KotlinUByte")))
@interface Ui_sharedUByte : Ui_sharedNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end

__attribute__((swift_name("KotlinShort")))
@interface Ui_sharedShort : Ui_sharedNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end

__attribute__((swift_name("KotlinUShort")))
@interface Ui_sharedUShort : Ui_sharedNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end

__attribute__((swift_name("KotlinInt")))
@interface Ui_sharedInt : Ui_sharedNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end

__attribute__((swift_name("KotlinUInt")))
@interface Ui_sharedUInt : Ui_sharedNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end

__attribute__((swift_name("KotlinLong")))
@interface Ui_sharedLong : Ui_sharedNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end

__attribute__((swift_name("KotlinULong")))
@interface Ui_sharedULong : Ui_sharedNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end

__attribute__((swift_name("KotlinFloat")))
@interface Ui_sharedFloat : Ui_sharedNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end

__attribute__((swift_name("KotlinDouble")))
@interface Ui_sharedDouble : Ui_sharedNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end

__attribute__((swift_name("KotlinBoolean")))
@interface Ui_sharedBoolean : Ui_sharedNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GenericItem")))
@interface Ui_sharedGenericItem : Ui_sharedBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWith_id:(Ui_sharedInt * _Nullable)_id _title:(NSString * _Nullable)_title _subtitle:(NSString * _Nullable)_subtitle __attribute__((swift_name("init(_id:_title:_subtitle:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWith_id:(Ui_sharedInt * _Nullable)_id _fullImage:(NSString * _Nullable)_fullImage _titleOverlay:(NSString * _Nullable)_titleOverlay _subtitleOverlay:(NSString * _Nullable)_subtitleOverlay __attribute__((swift_name("init(_id:_fullImage:_titleOverlay:_subtitleOverlay:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWith_id:(Ui_sharedInt * _Nullable)_id _topImage:(NSString * _Nullable)_topImage _title:(NSString * _Nullable)_title _subtitle:(NSString * _Nullable)_subtitle _rightDiscount:(NSString * _Nullable)_rightDiscount _rightPrice:(NSString * _Nullable)_rightPrice __attribute__((swift_name("init(_id:_topImage:_title:_subtitle:_rightDiscount:_rightPrice:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWith_id:(Ui_sharedInt * _Nullable)_id _leftImage:(NSString * _Nullable)_leftImage _title:(NSString * _Nullable)_title _subtitle:(NSString * _Nullable)_subtitle _middleDiscount:(NSString * _Nullable)_middleDiscount _middlePrice:(NSString * _Nullable)_middlePrice _middlePriceUnit:(NSString * _Nullable)_middlePriceUnit __attribute__((swift_name("init(_id:_leftImage:_title:_subtitle:_middleDiscount:_middlePrice:_middlePriceUnit:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithId:(Ui_sharedInt * _Nullable)id fullImage:(NSString * _Nullable)fullImage topImage:(NSString * _Nullable)topImage leftImage:(NSString * _Nullable)leftImage name:(NSString * _Nullable)name title:(NSString * _Nullable)title titleOverlay:(NSString * _Nullable)titleOverlay subtitle:(NSString * _Nullable)subtitle subtitleOverlay:(NSString * _Nullable)subtitleOverlay subtitleIconRes:(Ui_sharedInt * _Nullable)subtitleIconRes description:(NSString * _Nullable)description descriptionIconRes:(Ui_sharedInt * _Nullable)descriptionIconRes moreInfo:(NSString * _Nullable)moreInfo topTags:(NSMutableArray<NSString *> * _Nullable)topTags bottomTags:(NSMutableArray<NSString *> * _Nullable)bottomTags middleDiscount:(NSString * _Nullable)middleDiscount middlePrice:(NSString * _Nullable)middlePrice middlePriceUnit:(NSString * _Nullable)middlePriceUnit rightDiscount:(NSString * _Nullable)rightDiscount rightPrice:(NSString * _Nullable)rightPrice __attribute__((swift_name("init(id:fullImage:topImage:leftImage:name:title:titleOverlay:subtitle:subtitleOverlay:subtitleIconRes:description:descriptionIconRes:moreInfo:topTags:bottomTags:middleDiscount:middlePrice:middlePriceUnit:rightDiscount:rightPrice:)"))) __attribute__((objc_designated_initializer));
- (Ui_sharedInt * _Nullable)component1 __attribute__((swift_name("component1()"))) __attribute__((deprecated("use corresponding property instead")));
- (Ui_sharedInt * _Nullable)component10 __attribute__((swift_name("component10()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()"))) __attribute__((deprecated("use corresponding property instead")));
- (Ui_sharedInt * _Nullable)component12 __attribute__((swift_name("component12()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSMutableArray<NSString *> * _Nullable)component14 __attribute__((swift_name("component14()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSMutableArray<NSString *> * _Nullable)component15 __attribute__((swift_name("component15()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component16 __attribute__((swift_name("component16()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component17 __attribute__((swift_name("component17()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component18 __attribute__((swift_name("component18()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component19 __attribute__((swift_name("component19()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component20 __attribute__((swift_name("component20()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()"))) __attribute__((deprecated("use corresponding property instead")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()"))) __attribute__((deprecated("use corresponding property instead")));
- (Ui_sharedGenericItem *)doCopyId:(Ui_sharedInt * _Nullable)id fullImage:(NSString * _Nullable)fullImage topImage:(NSString * _Nullable)topImage leftImage:(NSString * _Nullable)leftImage name:(NSString * _Nullable)name title:(NSString * _Nullable)title titleOverlay:(NSString * _Nullable)titleOverlay subtitle:(NSString * _Nullable)subtitle subtitleOverlay:(NSString * _Nullable)subtitleOverlay subtitleIconRes:(Ui_sharedInt * _Nullable)subtitleIconRes description:(NSString * _Nullable)description descriptionIconRes:(Ui_sharedInt * _Nullable)descriptionIconRes moreInfo:(NSString * _Nullable)moreInfo topTags:(NSMutableArray<NSString *> * _Nullable)topTags bottomTags:(NSMutableArray<NSString *> * _Nullable)bottomTags middleDiscount:(NSString * _Nullable)middleDiscount middlePrice:(NSString * _Nullable)middlePrice middlePriceUnit:(NSString * _Nullable)middlePriceUnit rightDiscount:(NSString * _Nullable)rightDiscount rightPrice:(NSString * _Nullable)rightPrice __attribute__((swift_name("doCopy(id:fullImage:topImage:leftImage:name:title:titleOverlay:subtitle:subtitleOverlay:subtitleIconRes:description:descriptionIconRes:moreInfo:topTags:bottomTags:middleDiscount:middlePrice:middlePriceUnit:rightDiscount:rightPrice:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSMutableArray<NSString *> * _Nullable bottomTags __attribute__((swift_name("bottomTags")));
@property (setter=setDescription:) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property Ui_sharedInt * _Nullable descriptionIconRes __attribute__((swift_name("descriptionIconRes")));
@property NSString * _Nullable fullImage __attribute__((swift_name("fullImage")));
@property Ui_sharedInt * _Nullable id __attribute__((swift_name("id")));
@property NSString * _Nullable leftImage __attribute__((swift_name("leftImage")));
@property NSString * _Nullable middleDiscount __attribute__((swift_name("middleDiscount")));
@property NSString * _Nullable middlePrice __attribute__((swift_name("middlePrice")));
@property NSString * _Nullable middlePriceUnit __attribute__((swift_name("middlePriceUnit")));
@property NSString * _Nullable moreInfo __attribute__((swift_name("moreInfo")));
@property NSString * _Nullable name __attribute__((swift_name("name")));
@property NSString * _Nullable rightDiscount __attribute__((swift_name("rightDiscount")));
@property NSString * _Nullable rightPrice __attribute__((swift_name("rightPrice")));
@property NSString * _Nullable subtitle __attribute__((swift_name("subtitle")));
@property Ui_sharedInt * _Nullable subtitleIconRes __attribute__((swift_name("subtitleIconRes")));
@property NSString * _Nullable subtitleOverlay __attribute__((swift_name("subtitleOverlay")));
@property NSString * _Nullable title __attribute__((swift_name("title")));
@property NSString * _Nullable titleOverlay __attribute__((swift_name("titleOverlay")));
@property NSString * _Nullable topImage __attribute__((swift_name("topImage")));
@property NSMutableArray<NSString *> * _Nullable topTags __attribute__((swift_name("topTags")));
@end

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
