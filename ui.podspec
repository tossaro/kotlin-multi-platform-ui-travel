Pod::Spec.new do |spec|
    spec.name                  = 'ui'
    spec.version               = '0.1.0'
    spec.homepage              = 'https://gitlab.com/tossaro/kotlin-multi-platform-ui-travel'
    spec.source                = { :git => 'https://gitlab.com/tossaro/kotlin-multi-platform-ui-travel.git', :tag => spec.version.to_s }
    spec.license               = { :type => 'MIT', :file => 'LICENSE' }
    spec.summary               = 'Provide UI Style & Component'
    spec.authors      		   =  { 'tossaro' => 'hamzah.tossaro@gmail.com' }
    spec.source_files 		   = "ui_ios/ui/Classes/**/*.{swift}"
    spec.resources             = "ui_ios/ui/Resources/**/*.{gif,png,jpeg,jpg,storyboard,xib,xcassets,ttf,otf}"
    spec.libraries             = 'c++'
    spec.ios.deployment_target = '14.1'
    spec.static_framework      = true
    spec.dependency 'core'
    spec.dependency 'core_shared'
    spec.dependency 'ui_shared'
end
